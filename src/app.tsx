import * as preact from "preact";

const { h, render, Component } = preact

/** @jsx h */

interface Item {
  id: number
  title: string
  done: boolean
}

interface Filter {
  (item: Item): boolean
}

const FILTER: { [name: string]: Filter } = {
  ALL: () => true,
  ACTIVE: (item) => !item.done,
  COMPLETED: (item) => item.done,
}

interface AppState {
  items: Item[]
  filter: Filter
  nextId: number
  input: string
}

function valueOf(e: Event) {
  return (e.target as HTMLInputElement).value
}

class App extends Component<{}, AppState> {
  constructor() {
    super()

    this.setState({
      items: [],
      filter: FILTER.ALL,
      nextId: 1,
      input: ""
    })
  }

  get items() {
    return this.state.items
  }

  set items(items) {
    this.setState({
      items,
      nextId: items
        .map(i => i.id)
        .reduce((v, a) => Math.max(v, a), 0) + 1
    })

    this.onSetItems(items)
  }

  onSetItems(items: Item[]) { }

  delete(item: Item) {
    this.items = this.items.filter(i => i !== item)
  }

  toggle(item: Item) {
    item.done = !item.done
    this.forceUpdate()
  }

  clearCompleted() {
    this.setState({ items: this.items.filter(FILTER.ACTIVE) })
  }

  get filter() {
    return this.state.filter
  }

  set filter(filter) {
    this.setState({ filter })
  }

  get visible() {
    return this.items.filter(this.filter)
  }

  get remaining() {
    return this.items.filter(FILTER.ACTIVE).length
  }

  get input() {
    return this.state.input
  }

  set input(input) {
    this.setState({ input })
  }

  submit() {
    const items = this.items
    const id = this.state.nextId

    items.push({
      id,
      title: this.input,
      done: false
    })

    this.items = items

    this.setState({ input: "", nextId: id + 1 })
  }

  render() {
    return (
      <section class="todoapp">
        <header class="header">
          <h1>todos</h1>
          <input class="new-todo" placeholder="What needs to be done?" autofocus
            value={this.input}
            onInput={e => this.input = valueOf(e)}
            onKeyDown={e => e.keyCode === 13 ? this.submit() : true}
          />
        </header>
        <section class="main">
          <input class="toggle-all" type="checkbox" />
          <label for="toggle-all">Mark all as complete</label>
          <ul class="todo-list">
            <ul class="todo-list">
              {this.visible.map(item =>
                <li key={item.id + ""} class={item.done ? "completed" : ""}>
                  <input class="toggle" type="checkbox" checked={item.done} onClick={() => this.toggle(item)} />
                  <label>{item.title}</label>
                  <button class="destroy" onClick={() => this.delete(item)}></button>
                </li>
              )}
            </ul>
          </ul>
          <footer class="footer">
            <span class="todo-count">{this.remaining} items left</span>
            <div class="filters">
              <a onClick={() => this.filter = FILTER.ALL} class={this.filter === FILTER.ALL ? "active" : ""}>All</a>
              <a onClick={() => this.filter = FILTER.ACTIVE} class={this.filter === FILTER.ACTIVE ? "active" : ""}>Active</a>
              <a onClick={() => this.filter = FILTER.COMPLETED} class={this.filter === FILTER.COMPLETED ? "active" : ""}>Completed</a>
            </div>
            <button class="clear-completed" onClick={() => this.clearCompleted()}>Clear completed</button>
          </footer>
        </section>
      </section>
    )
  }
}

function connect(app: App) {
  app.onSetItems = items => localStorage.setItem("todo.items", JSON.stringify(app.items))

  const data = localStorage.getItem("todo.items")

  if (data) {
    try {
      app.items = JSON.parse(data)
      return
    } catch (_) { }
  }

  app.items = [
    { id: 1, title: "Collect underpants", done: true },
    { id: 2, title: "???", done: false },
    { id: 3, title: "Profit", done: false }
  ]
}

render(<App ref={connect} />, document.body)
