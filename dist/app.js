define(["require", "exports", "preact"], function (require, exports, preact) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const { h, render, Component } = preact;
    const FILTER = {
        ALL: () => true,
        ACTIVE: (item) => !item.done,
        COMPLETED: (item) => item.done,
    };
    function valueOf(e) {
        return e.target.value;
    }
    class App extends Component {
        constructor() {
            super();
            this.setState({
                items: [],
                filter: FILTER.ALL,
                nextId: 1,
                input: ""
            });
        }
        get items() {
            return this.state.items;
        }
        set items(items) {
            this.setState({
                items,
                nextId: items
                    .map(i => i.id)
                    .reduce((v, a) => Math.max(v, a), 0) + 1
            });
            this.onSetItems(items);
        }
        onSetItems(items) { }
        delete(item) {
            this.items = this.items.filter(i => i !== item);
        }
        toggle(item) {
            item.done = !item.done;
            this.forceUpdate();
        }
        clearCompleted() {
            this.setState({ items: this.items.filter(FILTER.ACTIVE) });
        }
        get filter() {
            return this.state.filter;
        }
        set filter(filter) {
            this.setState({ filter });
        }
        get visible() {
            return this.items.filter(this.filter);
        }
        get remaining() {
            return this.items.filter(FILTER.ACTIVE).length;
        }
        get input() {
            return this.state.input;
        }
        set input(input) {
            this.setState({ input });
        }
        submit() {
            const items = this.items;
            const id = this.state.nextId;
            items.push({
                id,
                title: this.input,
                done: false
            });
            this.items = items;
            this.setState({ input: "", nextId: id + 1 });
        }
        render() {
            return (h("section", { class: "todoapp" },
                h("header", { class: "header" },
                    h("h1", null, "todos"),
                    h("input", { class: "new-todo", placeholder: "What needs to be done?", autofocus: true, value: this.input, onInput: e => this.input = valueOf(e), onKeyDown: e => e.keyCode === 13 ? this.submit() : true })),
                h("section", { class: "main" },
                    h("input", { class: "toggle-all", type: "checkbox" }),
                    h("label", { for: "toggle-all" }, "Mark all as complete"),
                    h("ul", { class: "todo-list" },
                        h("ul", { class: "todo-list" }, this.visible.map(item => h("li", { key: item.id + "", class: item.done ? "completed" : "" },
                            h("input", { class: "toggle", type: "checkbox", checked: item.done, onClick: () => this.toggle(item) }),
                            h("label", null, item.title),
                            h("button", { class: "destroy", onClick: () => this.delete(item) }))))),
                    h("footer", { class: "footer" },
                        h("span", { class: "todo-count" },
                            this.remaining,
                            " items left"),
                        h("div", { class: "filters" },
                            h("a", { onClick: () => this.filter = FILTER.ALL, class: this.filter === FILTER.ALL ? "active" : "" }, "All"),
                            h("a", { onClick: () => this.filter = FILTER.ACTIVE, class: this.filter === FILTER.ACTIVE ? "active" : "" }, "Active"),
                            h("a", { onClick: () => this.filter = FILTER.COMPLETED, class: this.filter === FILTER.COMPLETED ? "active" : "" }, "Completed")),
                        h("button", { class: "clear-completed", onClick: () => this.clearCompleted() }, "Clear completed")))));
        }
    }
    function connect(app) {
        app.onSetItems = items => localStorage.setItem("todo.items", JSON.stringify(app.items));
        const data = localStorage.getItem("todo.items");
        if (data) {
            try {
                app.items = JSON.parse(data);
                return;
            }
            catch (_) { }
        }
        app.items = [
            { id: 1, title: "Collect underpants", done: true },
            { id: 2, title: "???", done: false },
            { id: 3, title: "Profit", done: false }
        ];
    }
    render(h(App, { ref: connect }), document.body);
});
